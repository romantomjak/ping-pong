/* 
 * File:   main.cpp
 * Author: r00m
 *
 * Created on 19 November 2011, 14:16
 */

#include <cstdio>
#include <conio.h>
#include <windows.h>
#include <process.h>

using namespace std;

struct Pad {
  int x;
  int y1;
  int y2;
};

struct Ball {
  int x;
  int y;
  int xh;
  int yh;
};

void initPads(const Pad &, const Pad &);
int movePad(Pad &, int, int);
void moveBall(void *);

bool continueGame = true;
Ball ball = {32, 15, -1, -1};
Pad leftPad = {0, 5, 9};
Pad rightPad = {79, 2, 6};

int main() {

  char key = 'a';

  // draw both pads;
  initPads(leftPad, rightPad);

  // launch thread to move ball
  _beginthread(moveBall, 0, NULL );



  do {
    key = getch();

    switch(key) {
      case 'w':
        movePad(leftPad, leftPad.y1-1, leftPad.y2-1);
        break;

      case 's':
        movePad(leftPad, leftPad.y1+1, leftPad.y2+1);
        break;

      case 'a':
        movePad(rightPad, rightPad.y1-1, rightPad.y2-1);
        break;

      case 'd':
        movePad(rightPad, rightPad.y1+1, rightPad.y2+1);
        break;

    }

  } while(key != 'q');


  system("cls");


  return 0;
}



void moveBall(void *) {

  while(continueGame) {
    HANDLE hOut;
    COORD Position;

    hOut = GetStdHandle(STD_OUTPUT_HANDLE);


    // exit game if user failed to catch the ball
    if(ball.x == 0 || ball.x == 79) {
      //continueGame = false;
      //_endthread();
      exit(0);
    }


    // reverse ball's y-heading when hit ceiling/floor
    if(ball.y == 0 || ball.y == 24)
      ball.yh = -ball.yh;


    // check if user catched the ball
    if((ball.y >= leftPad.y1 && ball.y <= leftPad.y2 && ball.x+ball.xh == leftPad.x) ||
       (ball.y >= rightPad.y1 && ball.y <= rightPad.y2 && ball.x+ball.xh == rightPad.x)
      )
      ball.xh = -ball.xh;


    // delete old ball
    Position.X = ball.x;
    Position.Y = ball.y;
    SetConsoleCursorPosition(hOut, Position);
    printf(" ");


    // update ball's location
    ball.x += ball.xh;
    ball.y += ball.yh;


    // draw new ball
    Position.X = ball.x;
    Position.Y = ball.y;
    SetConsoleCursorPosition(hOut, Position);
    printf("o");


    Sleep(110);
  }

  //_endthread();
  exit(0);
}

int movePad(Pad &pad, int ny1, int ny2) {
  HANDLE hOut;
  COORD Position;

  hOut = GetStdHandle(STD_OUTPUT_HANDLE);


  // exit if out of field
  if(ny1 == -1 || ny1 == 22) {
    Beep(750,300);
    return 1;
  }


  // delete pad
  if(pad.y1 > ny1) {

    // moved up
    Position.X = pad.x;
    Position.Y = pad.y2-1;
  } else {

    // moved down
    Position.X = pad.x;
    Position.Y = pad.y1;
  }

  SetConsoleCursorPosition(hOut, Position);
  printf(" ");


  // move pad
  for(int i = ny1; i < ny2; i++) {

    Position.X = pad.x;
    Position.Y = i;
    SetConsoleCursorPosition(hOut, Position);

    printf("#");
  }


  // reset cursor position
  Position.X = 0;
  Position.Y = 0;
  SetConsoleCursorPosition(hOut, Position);


  // set new pad position
  pad.y1 = ny1;
  pad.y2 = ny2;

  return 0;
}

void initPads(const Pad &leftPad, const Pad &rightPad) {
  HANDLE hOut;
  COORD Position;
  CONSOLE_CURSOR_INFO CCI {1, false}; // DWORD dwSize, BOOL bVisible

  hOut = GetStdHandle(STD_OUTPUT_HANDLE);

  // draw left pad
  for(int i = leftPad.y1; i < leftPad.y2; i++) {
    Position.X = 0;
    Position.Y = i;
    SetConsoleCursorPosition(hOut, Position);

    printf("#");
  }

  // draw right pad
  for(int i = rightPad.y1; i < rightPad.y2; i++) {
    Position.X = 79;
    Position.Y = i;
    SetConsoleCursorPosition(hOut, Position);

    printf("#");
  }

  // hide cursor
  SetConsoleCursorInfo(hOut, &CCI);
}